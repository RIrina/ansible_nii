# Общие роли для Ansible для реализации IaC

## Common

Общая роль для всех хостов, создает пользователя devops, добавляет его в sudoers, копирует публичный ключ в authorized_keys

## Proxy

Настройки реверс-прокси на хосте GATEWAY с перезагрузкой конфигурации nginx. Для добавления настроек хоста, достаточно положить конфиг в ./roles/proxy/files и запустить соответствующий плейбук

## Nextcloud 

Роль для nextcloud, в данный момент просто копирует конфиг для nginx, относящийся к nextcloud с перезагрузкой конфигурации nginx

## Плейбуки

common.yml - развертывание роли common на все хосты
nginx.yml - роли nextcloud + proxy

## Запуск

Запуск common:

```sh
ansible-playbook -i inventory/inv common.yml
```

Запуск nginx:
```sh
ansible-playbook -i inventory/inv nginx.yml
```
